<!DOCTYPE html>
<html lang="ru-RU">

    <head>
        <title>Product</title>
        <?php include 'parts/main/head.php'; ?>
    </head>

    <body>
        <div class="page__wrap js__page__wrap">
            <?php include 'parts/main/header.php'; ?>

            <div class="breadcrumbs">
                <div class="container">
                    <ul class="breadcrumbs__list">
                        <li><a href="">Naturtek</a></li>
                        <li><a href="">Каталог</a></li>
                        <li><a href="">Для стирки</a></li>
                        <li>Экопятновыводитель для жирных пятен</li>
                    </ul>
                </div>
            </div>

            <div class="product__page">
                <div class="container">
                    <div class="product__page__inner">

                        <?php include 'parts/product/image.php'; ?>
                        <?php include 'parts/product/info.php'; ?>

                    </div>
                </div>
            </div>

            <?php include 'parts/product/recomendate.php'; ?>



            <?php include 'parts/main/footer.php'; ?>
        </div>
    </body>

</html>
