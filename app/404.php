<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title>Category</title>
        <?php include 'parts/main/head.php'; ?>
    </head>

    <body>
        <div class="page__wrap js__page__wrap">
            <?php include 'parts/main/header.php'; ?>
            <div class="content__404">
                <div class="container">
                    <div class="content__404__inner">
                        <div class="content__404__title">404</div>
                        <div class="content__404__text">
                            <p>Знаменитое выражение Декарта «Cogito ergo sum» — «Я мыслю, следовательно, я существую», — коротко говоря, о том, что можно сомневаться во всем, кроме собственного существования; если же ты вдруг перестаешь думать, то исчезаешь моментально.</p>

                            <p>Задумывались вы когда-нибудь, глядя на человека, о чем он сейчас думает? На самом деле, это не так уж и важно: главное, что если он мыс­лит — значит, он есть.</p>

                            <p>В нашей работе мы всегда думаем, думаем о людях и о том, как мы можем им помочь обрести гармонию с окружающим миром.</p>

                            <p>Так или иначе, этой страницы не существует.</p>
                        </div>
                    </div>
                </div>
            </div>



            <?php include 'parts/main/footer.php'; ?>
        </div>
    </body>
</html>