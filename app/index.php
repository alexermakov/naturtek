<!DOCTYPE html>
<html lang="ru-RU">

    <head>
        <title>Index</title>
        <?php include 'parts/main/head.php'; ?>
    </head>
    <?php $home = true;?>
    <body class='body__home'>
        <div class="page__wrap js__page__wrap">
            <?php include 'parts/main/header.php'; ?>




            <?php include 'parts/home/promo.php'; ?>
            <?php include 'parts/home/about_0.php'; ?>
            <?php include 'parts/home/catalog.php'; ?>
            <?php include 'parts/home/about.php'; ?>
            <?php include 'parts/home/where_buy.php'; ?>
            <?php include 'parts/home/social.php'; ?>





            <?php include 'parts/main/footer.php'; ?>
        </div>
    </body>

</html>
