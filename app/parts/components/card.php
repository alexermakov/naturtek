<div class="product__card">
    <div class="product__card__image">
        <a href="product.php" class="product__card__product__link"></a>
        <div class="product__card__image__inner">
            <img src="img/__content/product.jpg">
        </div>
        <a href="product.php" class="product__card__add__to__cart">Подробнее</a>
    </div>
    <div class="product__card__info">
        <h4 class="product__card__title">
            <a href="product.php">Экопятновыводитель для жирных пятен</a>
        </h4>
        <div class="product__card__info_2">
            <span class="product__card__size">1400 мл</span>
            <div class="product__card__dot">●</div>
        </div>
    </div>
</div>