<div class="home__about" id='about'>
    <div class="container">
        <div class="home__about__title js__animate__opacity__big__move">С Naturtek вам никогда не придётся выбирать между экологичностью и эффективностью
        </div>
        <div class="home__about__line"></div>
        <div class="home__about__mission">
            <div class="home__about__mission__col_1">
                <div class="home__about__mission__title js__animate__opacity__big__move">Наша миссия</div>
                <div class="home__about__mission__text js__animate__opacity__big__move">
                    <p><b>ищем новые смарт-решения, чтобы каждый из наших продуктов избавлял вас от необходимости идти на компромисс между нужным результатом и заботой о вашем здоровье.</b></p>
                    <p>Применяем современные технологии, чтобы этично получать от природы лучшее, что она может дать. На основе натуральных природных компонентов мы создаем эффективные средства, которые тщательно продуманы и разработаны так, чтобы превосходить ваши ожидания.</p>
                </div>

                <div class="home__about__mission__image__2 home__about__mission__image__2--mobile js__animate__opacity__big__move">
                    <div class="home__about__mission__image__2__inner">
                        <img src="img/pages/home/about/2.jpg">
                    </div>
                    <div class="home__about__mission__image__2__text">Откройте лучшее, что есть в природе</div>
                </div>

                <div class="home__about__mission__image__1">
                    <div class="home__about__mission__image__1__wrap">
                        <div class="home__about__mission__image__1__inner js__animate__image__about">
                            <img src="img/pages/home/about/1.jpg">
                        </div>
                        <div class="home__about__mission__image__1__text">безопасно для детей и аллергиков</div>
                    </div>
                </div>
                <div class="home__about__mission__add">
                    <div class="home__about__mission__add__text js__animate__opacity__big__move">Создаем инновационные продукты с высокими техническими характеристиками</div>
                    <a href="category.php" class="btn__default btn_add_mission js__animate__opacity__big__move">
                        <div class="btn_add_mission__text">В каталог</div>
                        <div class="btn_add_mission__icon">
                            <svg width="36" height="18" viewBox="0 0 36 18" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0_610_3030)">
                                    <path
                                        d="M22.6749 -0.5H21.446C21.446 3.1 24.5181 6.22 28.9419 7.9H0V9.1H28.819C24.3952 10.78 21.3232 13.9 21.3232 17.5H22.552C22.552 12.82 28.4503 9.1 35.8232 9.1V7.9C28.5732 7.9 22.6749 4.18 22.6749 -0.5Z"
                                        fill="#F3F2EC" />
                                </g>
                                <defs>
                                    <clipPath id="clip0_610_3030">
                                        <rect width="36" height="18" fill="#F3F2EC" transform="translate(0 -0.5)" />
                                    </clipPath>
                                </defs>
                            </svg>

                            <svg width="36" height="18" viewBox="0 0 36 18" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0_610_3030)">
                                    <path
                                        d="M22.6749 -0.5H21.446C21.446 3.1 24.5181 6.22 28.9419 7.9H0V9.1H28.819C24.3952 10.78 21.3232 13.9 21.3232 17.5H22.552C22.552 12.82 28.4503 9.1 35.8232 9.1V7.9C28.5732 7.9 22.6749 4.18 22.6749 -0.5Z"
                                        fill="white" />
                                </g>
                                <defs>
                                    <clipPath id="clip0_610_3030">
                                        <rect width="36" height="18" fill="white" transform="translate(0 -0.5)" />
                                    </clipPath>
                                </defs>
                            </svg>

                        </div>
                    </a>
                </div>
            </div>
            <div class="home__about__mission__col_2">
                <div class="home__about__mission__image__2">
                    <div class="home__about__mission__image__2__inner js__animate__image__about">
                        <img src="img/pages/home/about/2.jpg">
                    </div>
                    <div class="home__about__mission__image__2__text">Откройте лучшее, что есть в природе</div>
                </div>
            </div>
        </div>

        <div class="home__about__feature js__animate__opacity__big__move">
            <div class="home__about__feature__item">
                <div class="home__about__feature__item__title">Гипоаллергенно и безопасно</div>
                <div class="home__about__feature__item__text">Средства NATURTEK с успехом проходят клинические испытания
                    и не вызывают аллергических реакций</div>
            </div>
            <div class="home__about__feature__item">
                <div class="home__about__feature__item__title">Перерабатываемая упаковка</div>
                <div class="home__about__feature__item__text">Флаконы средств NATURTEK имеют маркировку PET или HDPE, а
                    значит подлежат переработке.</div>
            </div>
            <div class="home__about__feature__item">
                <div class="home__about__feature__item__title">Полностью смывается водой</div>
                <div class="home__about__feature__item__text">Средства NATURTEK полностью смываются меньшим количеством
                    воды, не оставляя после себя плёнок, запаха и разводов</div>
            </div>
            <div class="home__about__feature__item">
                <div class="home__about__feature__item__title">Безопасно для септиков</div>
                <div class="home__about__feature__item__text">В основе экосредств нет токсичных компонентов, которые
                    уничтожают бактериальные культуры в автономных коттеджных канализациях.</div>
            </div>
        </div>
    </div>
</div>
