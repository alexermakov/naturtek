<?php
    function  home__catalog__block__info($title,$position){

        echo
        '<div class="home__catalog__block__info home__catalog__block__info--'.$position.'">
            <div class="home__catalog__block__info__inner">
                <img src="img/icons/cat__bg_0.svg" class="home__catalog__block__info__image">
                <div class="home__catalog__block__info_x">
                    <div class="home__catalog__block__text">
                        <div class="home__catalog__block__title"><a href="category.php">'.$title.'</a></div>
                        <div class="home__catalog__block__count">'.rand(10,1000).'</div>
                    </div>
                    <div class="home__catalog__block__arrow js_home__catalog__block__arrow">
                        <button class="home__catalog__block__arrow--prev">
                            <svg width="15" height="18" viewBox="0 0 15 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M13.1561 18L14.385 18C14.385 14.4 11.3129 11.28 6.88914 9.6C6 9.26233 5.5 9 5.5 9L7.012 8.4C11.4358 6.72 14.5078 3.6 14.5078 0L13.279 -1.07427e-07C13.279 4.68 7.38066 8.4 0.00778091 8.4L0.00778081 9.6C7.2578 9.6 13.1561 13.32 13.1561 18Z" fill="#1D1D1D"/>
                            </svg>
                        </button>

                        <button class="home__catalog__block__arrow--next">
                            <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6.84387 0L5.61505 0C5.61505 3.6 8.68715 6.72 13.1109 8.4C14 8.73767 14.5 9 14.5 9L12.988 9.6C8.56421 11.28 5.49219 14.4 5.49219 18H6.72101C6.72101 13.32 12.6193 9.6 19.9922 9.6V8.4C12.7422 8.4 6.84387 4.68 6.84387 0Z" fill="#1D1D1D"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>';
    }
?>

<div class="home__catalog">
    <div class="container">
        <div class="home__catalog__top__line js__animate__opacity">
            <div class="home__catalog__title">Каталог</div>
            <a href="category.php" class="home__catalog__top__link">Смотреть все товары</a>
        </div>
        <div class="home__catalog__block__inner">

            <div class="home__catalog__block js__home__catalog__block js__animate__opacity">
                <?= home__catalog__block__info('Стирка','2');?>
                <div class="home__catalog__block__list js_home__catalog__block__list swiper">
                    <div class="swiper-wrapper">
                        <?php for ($i=0; $i < 6; $i++):?>
                            <div class="home__catalog__block__slide swiper-slide">
                                <?php include 'parts/components/card.php'; ?>
                            </div>
                        <?php endfor;?>
                    </div>
                </div>
            </div>

            <div class="home__catalog__block js__home__catalog__block js__animate__opacity">
                <?= home__catalog__block__info('Уборка','1');?>
                <div class="home__catalog__block__list js_home__catalog__block__list swiper">
                    <div class="swiper-wrapper">
                        <?php for ($i=0; $i < 6; $i++):?>
                            <div class="home__catalog__block__slide swiper-slide">
                                <?php include 'parts/components/card.php'; ?>
                            </div>
                        <?php endfor;?>
                    </div>
                </div>
            </div>

            <div class="home__catalog__block js__home__catalog__block js__animate__opacity">
                <?= home__catalog__block__info('Посуда','3');?>
                <div class="home__catalog__block__list js_home__catalog__block__list swiper">
                    <div class="swiper-wrapper">
                        <?php for ($i=0; $i < 6; $i++):?>
                            <div class="home__catalog__block__slide swiper-slide">
                                <?php include 'parts/components/card.php'; ?>
                            </div>
                        <?php endfor;?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>