<div class="home__promo js__home__promo__wrap">
    <div class="container">
        <div class="home__promo__inner js_home__promo">
            <div class="home__promo__inner__wrap">
                <div class="home__promo__list">
                    <a href="category.php" data-color='#A7AFD1' class='active'>
                        <span class="home__promo__title">Стирка</span>
                    </a>
                    <a href="category.php" data-color='#A6D1B7'>
                        <span class="home__promo__title">Уборка</span>
                    </a>
                    <a href="category.php" data-color='#D1AEA6'>
                        <span class="home__promo__title">Для посуды</span>
                    </a>
                    <a href="category.php" data-color='#D1CFA6'>
                        <span class="home__promo__title">Для детей</span>
                    </a>
                    <a href="category.php" data-color='#A6C2D1'>
                        <span class="home__promo__title">Косметика</span>
                    </a>
                </div>

                <div class="home__promo__list__image">
                    <div class="home__promo__list__image__item active">
                        <img src="img/__content/home/promo/1.png">
                    </div>
                    <div class="home__promo__list__image__item">
                        <img src="img/__content/home/promo/2.png">
                    </div>
                    <div class="home__promo__list__image__item">
                        <img src="img/__content/home/promo/1.png">
                    </div>
                    <div class="home__promo__list__image__item">
                        <img src="img/__content/home/promo/2.png">
                    </div>
                    <div class="home__promo__list__image__item">
                        <img src="img/__content/home/promo/1.png">
                    </div>
                </div>
            </div>
            <div class="home__promo__text">
                Российский эко-технологичный<br>
                бренд косметики и бытовой химии
            </div>
        </div>
    </div>
</div>