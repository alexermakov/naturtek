<div class="product__page__images">

    <div class="product__page__images__main js_product__page__images__main">
        <?php for ($i=0; $i < 3; $i++):?>
            <div class="product__page__images__main__item">
                <div class="product__page__images__main__item__inner">
                    <img src="img/__content/product_2.jpg">
                </div>
            </div>
        <?php endfor;?>
    </div>

    <div class="product__page__add__main js_product__page__images__add">
        <?php for ($i=0; $i < 3; $i++):?>
            <a href='#<?= $i;?>' class="product__page__images__add__item<?php if ($i == 0):?> active<?php endif;?>">
                <div class="product__page__images__add__item__inner">
                    <img src="img/__content/product.jpg">
                </div>
            </a>
        <?php endfor;?>
    </div>

</div>
