document.addEventListener("DOMContentLoaded", function () {
    $('body').addClass('loaded')
});

$(function () {


    $('.js__anchor').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            'scrollTop': $($(this).attr('href')).offset().top
        }, 2000);
    });

    $(window).scroll(function () {

        if ($(window).scrollTop()>0){
            let height = $('.js_header__main').outerHeight();
            $('.js_header__main').addClass('moved');
        }
        else{
            $('.js_header__main').removeClass('moved');
        }

        if ($(window).scrollTop()>$(window).height()){
            $('.js__btn__up').addClass('show')
        }
        else{
            $('.js__btn__up').removeClass('show')
        }
    });

    $('.js__btn__up').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            'scrollTop': 0
        }, 2000);
    });




    $('.js_mobile__menu__link').click(function (e) {
        e.preventDefault();
        $($('.js_btn_menu')[0]).trigger('click')
        $('html, body').animate({
            'scrollTop': $($(this).attr('href')).offset().top
        }, 2000);
    });



    const TIMEOUT = 5000;
    if (window.isMobile) TIMEOUT = 3500;


    let sliderAutoplayInterval;

    function sliderPlay() {

        sliderAutoplayInterval = setInterval(() => {
            elIndex = ($('.js_home__promo .home__promo__list a.active').index() + 1) % $('.js_home__promo .home__promo__list a').length
            changeSliderNext(elIndex)
        }, TIMEOUT);
    }


    function changeSliderNext(nextEl) {

        let el = $('.js_home__promo .home__promo__list a').eq(nextEl)
        $('.js_home__promo .home__promo__list a').removeClass('active')
        el.addClass('active')
        let index = el.index()

        $('.js_home__promo .home__promo__list__image__item.active').removeClass('active');
        $('.js_home__promo .home__promo__list__image__item').eq(index).addClass('active');

        $('.js__home__promo__wrap').css('background-color', el.data('color'))
    }

    $('.js_home__promo .home__promo__list a').hover(function (e) {
        e.preventDefault();
        clearInterval(sliderAutoplayInterval);
        setTimeout(() => {
            sliderPlay()
        }, 0);
        if (!$(this).hasClass('active')) {
            $('.js_home__promo .home__promo__list a').removeClass('active')
            $(this).addClass('active')
            let index = $(this).index()

            $('.js_home__promo .home__promo__list__image__item.active').removeClass('active');
            $('.js_home__promo .home__promo__list__image__item').eq(index).addClass('active');

            $('.js__home__promo__wrap').css('background-color', $(this).data('color'))
        }

    }, function () {
        // out
    });


    sliderPlay()







    Fancybox.bind('.js__modal', {
        autoFocus: false,
        trapFocus: false,
        closeButton: 'outside',
    });


    $('.js__btn_close__modal').click(function (e) {
        Fancybox.close();
    });

    let slickProduct = $('.js_product__page__images__main');

    slickProduct.on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $('.js_product__page__images__add a').eq(currentSlide).trigger('click')
    });


    $('.js_home__catalog__block__list').each(function (index, element) {
        let that = $(this);

        new Swiper(that[0], {
            slidesPerView: 2,
            autoHeight: true,
            spaceBetween: 0,
            loop: true,
            speed: 1300,

            navigation: {
                prevEl: that.parents('.home__catalog__block').find('.js_home__catalog__block__arrow button:first-child')[0],
                nextEl: that.parents('.home__catalog__block').find('.js_home__catalog__block__arrow button:last-child')[0],
            },

            breakpoints: {
                700: {
                    slidesPerView: 3,
                    spaceBetween: 0,
                },
                1200: {
                    slidesPerView: 4,
                    spaceBetween: 0,
                }
            }
        });
    })


    $(".js__animate__title").each(function () {
        el = $(this).find('span')[0]
        let height = $(el).height() + 20

        gsap.from(el, 0.8, {
            scrollTrigger: {
                trigger: el,
                start: 'top 100%',
            },
            y: height,
            ease: "cubic-bezier(0.38, 0.005, 0.215, 1)",
            delay: 0,
            stagger: {
                amount: 0.3
            }
        })
    })

    $(".js__animate__opacity__not__move").each(function () {
        el = $(this)[0];
        gsap.from(el, 1.2, {
            scrollTrigger: {
                trigger: el,
                start: 'top 90%',
            },
            opacity: 0,
            ease: "power4.out",
            delay: 0.25,
            stagger: {
                amount: 0.3
            }

        })
    })

    $(".js__animate__opacity").each(function () {
        el = $(this)[0];
        gsap.from(el, 1.2, {
            scrollTrigger: {
                trigger: el,
                start: 'top 90%',
            },
            y: 20,
            opacity: 0,
            ease: "power4.out",
            delay: 0.25,
            stagger: {
                amount: 0.3
            }

        })
    })
    $(".js__animate__opacity__big__move").each(function () {
        el = $(this)[0];
        gsap.from(el, 1.2, {
            scrollTrigger: {
                trigger: el,
                start: 'top 90%',
            },
            y: 50,
            opacity: 0,
            ease: "power4.out",
            delay: 0.25,
            stagger: {
                amount: 0.3
            }

        })
    })


    $('.js__animate__image__about img').each(function () {
        el = $(this)[0];
        gsap.from(el, 1.25, {
            scrollTrigger: {
                trigger: el,
                start: 'top 90%',
            },
            scale: 1.35,
            delay: 0.25,
            ease: "cubic-bezier(0.38, 0.005, 0.215, 1)",
            stagger: {
                amount: 0.3
            }

        })
    })

    $(".js__animate__opacity_0").each(function () {
        el = $(this)[0];
        elParent = $(this).parent()[0];

        gsap.from(el, 1, {
            scrollTrigger: {
                trigger: elParent,
                start: 'top 85%',
            },
            y: 10,
            opacity: 0,
            delay: 0.25,
            ease: "power4.out",
            stagger: {
                amount: 0.3
            }

        })
    })


    $('.js__home__catalog__block').mouseleave(function (elem) {
        let controls = $(this).find('.js_home__catalog__block__arrow');
        gsap.to(controls, 0.75, {
            y: 0
        })
    })



    $('.js__home__catalog__block').mousemove(function (e) {
        f = throttle(moveControl, 100)
        f($(this), e)
    })




    function throttle(func, ms) {

        let isThrottled = false,
            savedArgs,
            savedThis;

        function wrapper() {

            if (isThrottled) { // (2)
                savedArgs = arguments;
                savedThis = this;
                return;
            }

            func.apply(this, arguments); // (1)

            isThrottled = true;

            setTimeout(function () {
                isThrottled = false; // (3)
                if (savedArgs) {
                    wrapper.apply(savedThis, savedArgs);
                    savedArgs = savedThis = null;
                }
            }, ms);
        }

        return wrapper;
    }



    function moveControl(elem, mouse) {
        if (window.isMobile) return


        let heightArrow = parseInt($('.js_home__catalog__block__arrow').outerHeight());
        let rect = elem.find('.home__catalog__block__info__inner')[0].getBoundingClientRect();
        let y = mouse.clientY - rect.top - rect.height + heightArrow / 2;

        if (y > 0) y = 0;
        if (y <= -(rect.height - heightArrow)) y = -(rect.height - heightArrow);

        let controls = elem.find('.js_home__catalog__block__arrow');

        gsap.to(controls, 0.5, {
            y: y
        })


    }







    slickProduct.slick({
        dots: false,
        arrows: false,
        responsive: [{
                breakpoint: 871,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    })



    $('.js_product__page__images__add a').click(function (e) {
        e.preventDefault();
        $('.js_product__page__images__add a').removeClass('active');
        $(this).addClass('active');
        let index = $(this).attr('href').substring(1);
        slickProduct.slick('slickGoTo', index)
    })






    $('.js_product__page__tab__btn').click(function (e) {
        e.preventDefault();
        $(this).find('.product__page__tab__btn').toggleClass('active')
        $(this).closest('.js__product__page__tab').find('.product__page__tab__content').slideToggle(400)

    })


    // $('.js_home__promo .home__promo__list a').click(function (e) {
    //     e.preventDefault();
    //     if (!$(this).hasClass('active')){
    //         $('.js_home__promo .home__promo__list a').removeClass('active')
    //         $(this).addClass('active')
    //         let index = $(this).index()

    //         $('.js_home__promo .home__promo__list__image__item.active').removeClass('active');
    //         $('.js_home__promo .home__promo__list__image__item').eq(index).addClass('active');

    //         $('.js__home__promo__wrap').css('background-color', $(this).data('color'))
    //     }
    // })







    $('.js__marquee').each(function () {
        var obj = $(this);
        var d = obj.width();
        obj.clone().appendTo(obj.parent());
        obj.parent().parent().width(d);

        var t = d / 70;

        TweenMax.to(
            obj.parent(),
            t, // our calculated time
            {
                x: "-" + (d + 4),
                ease: Linear.easeNone,
                repeat: -1,
            }
        );

    });




    $('.js_btn_menu,.js_modal__menu__btn_close,.js_modal__menu__overlay').click(function (e) {
        e.preventDefault();
        $('.js_btn_menu').toggleClass('active')
        $('.header__main').toggleClass('open_mobile_menu')
        $('.js__modal__menu').toggleClass('active')
        $('.js_modal__menu__overlay').toggleClass('active')
    });



});