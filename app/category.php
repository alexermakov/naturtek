<!DOCTYPE html>
<html lang="ru-RU">
    <head>
        <title>Category</title>
        <?php include 'parts/main/head.php'; ?>
    </head>

    <body>
        <div class="page__wrap js__page__wrap">
            <?php include 'parts/main/header.php'; ?>

            <div class="breadcrumbs">
                <div class="container">
                    <ul class="breadcrumbs__list">
                        <li><a href="">Naturtek</a></li>
                        <li>Каталог</li>
                    </ul>
                </div>
            </div>

            <div class="category__top">
                <div class="container">
                    <div class="category__top__list">
                        <a href="" class='active'>Стирка</a>
                        <a href="">Уборка</a>
                        <a href="">Для посуды</a>
                        <a href="">Для детей</a>
                        <a href="">Косметика</a>
                    </div>

                    <div class="category__top__tags">
                        <a href="" class='active'>Все</a>
                        <a href="">Стиральные порошки</a>
                        <a href="">Гели для стирки</a>
                        <a href="">Кондиционеры</a>
                        <a href="">Таблетки для ППМ</a>
                    </div>
                </div>
            </div>

            <div class="category__inner">
                <div class="container">
                    <h1 class="title_x">Стирка</h1>
                    <div class="category__list">
                        <div class="product__list">
                            <?php for ($i=0; $i < 12; $i++):?>
                                <?php include 'parts/components/card.php'; ?>
                            <?php endfor;?>
                        </div>
                    </div>
                </div>
            </div>

            <?php include 'parts/components/pagination.php'; ?>




            <?php include 'parts/main/footer.php'; ?>
        </div>
    </body>
</html>