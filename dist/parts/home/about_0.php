<div class="home__about_0">
    <div class="container">
        <div class="home__about_0__title">
            <div class="js__animate__title animate__title">
                <span><b>Naturtek</b> — баланс</span>
            </div>
            <div class="js__animate__title animate__title">
                <span>эффективности и&nbsp;натуральности</span>
            </div>
        </div>
        <div class="home__about_0__text js__animate__opacity_0">Мы используем весь потенциал науки в сочетании с принципами бережного отношения к природе и человеку, чтобы создавать инновационные продукты для ухода за собой и поддержания чистоты в доме.</div>
        <div class="home__about_0__list">
            <div class="home__about_0__list__col_1">
                <div class="home__about_0__list__item js__animate__image__about">
                    <img src="img/pages/home/about/3.jpg" class="home__about_0__list__item__img">
                </div>
            </div>
            <div class="home__about_0__list__col_2">
                <div class="home__about_0__list__item home__about_0__list__item--2">
                    <div class="home__about_0__list__item__text">С Naturtek вам никогда не придется выбирать между экологичностью и эффективностью!</div>
                    <div class="home__about_0__list__item__wrap js__animate__image__about">
                        <img src="img/pages/home/about/4.jpg" class="home__about_0__list__item__img">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>