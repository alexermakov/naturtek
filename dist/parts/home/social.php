<div class="home__social" id='contact'>
    <div class="container">
        <div class="home__social__info">
            <div class="home__social__title">
                <div class="js__animate__title animate__title">
                    <span>Присоединяйтесь</span>
                </div>
                <div class="js__animate__title animate__title">
                    <span>к нам в соцсетях</span>
                </div>
            </div>
            <div class="home__social__text js__animate__opacity__big__move">Более 500 000 человек уже следят за нашими новостями и акциями в социальных сетях</div>
            <div class="home__social__row js__animate__opacity__big__move">
                <a href="" target="_blank" class="home__social__link">Одноклассники</a>
                <a href="" target="_blank" class="home__social__link">Вконтакте</a>
            </div>
        </div>
    </div>
</div>