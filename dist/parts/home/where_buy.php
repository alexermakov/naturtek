<div class="home__where__buy" id='buy'>
    <div class="container">
        <div class="home__where__buy__inner">
            <div class="home__where__buy__marque__wrap js__animate__opacity">
                <div class="home__where__buy__marque">
                    <div class="js__marquee marquee">
                        где купить где купить где купить где купить где купить
                    </div>
                </div>
            </div>
            <div class="home__where__buy__list js__animate__opacity__not__move">
                <?php for ($i=1; $i <= 10; $i++) :?>
                    <div class="home__where__buy__item">
                        <img src="img/pages/home/logo/<?= $i;?>.png?0">
                    </div>
                <?php endfor;?>
            </div>
            <div class="home__where__buy__add__text js__animate__opacity__big__move">Нажмите на нужный магазин, чтобы перейти к покупкам или ищите на полках в магазинах вашего города</div>
        </div>
    </div>
</div>