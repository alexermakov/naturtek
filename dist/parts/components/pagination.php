<div class="pagination__block">
    <div class="container">
        <a href="" class="pagination__link__prev pagination__link pagination__link--dissable">
            <svg width="36" height="19" viewBox="0 0 36 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.3251 0.5H14.554C14.554 4.1 11.4819 7.22 7.05815 8.9H36V10.1H7.18101C11.6048 11.78 14.6768 14.9 14.6768 18.5H13.448C13.448 13.82 7.54966 10.1 0.176788 10.1V8.9C7.4268 8.9 13.3251 5.18 13.3251 0.5Z" fill="#1D1D1D"/>
            </svg>
            Назад
        </a>

        <div class="pagination__block__center">
            <ul>
                <li><a href="" class='active'>1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li>
                <li><a href="">4</a></li>
                <li><span>...</span></li>
                <li><a href="">5</a></li>
            </ul>
        </div>

        <a href="" class="pagination__link__next pagination__link">
            Вперёд
            <svg width="36" height="19" viewBox="0 0 36 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M22.6749 0.5H21.446C21.446 4.1 24.5181 7.22 28.9419 8.9H0V10.1H28.819C24.3952 11.78 21.3232 14.9 21.3232 18.5H22.552C22.552 13.82 28.4503 10.1 35.8232 10.1V8.9C28.5732 8.9 22.6749 5.18 22.6749 0.5Z" fill="#1D1D1D"/>
            </svg>
        </a>
    </div>
</div>