<div class="product__page__info">
    <h1 class="product__page__title">Экологичный стиральный порошок для детского белья, алое и гортензия</h1>
    <div class="product__page__actions">
        <a href='' class="product__page__btn">
            <div class="product__page__btn__text">Ozon</div>
            <div class="product__page__btn__icon">
                <svg width="36" height="19" viewBox="0 0 36 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M22.6749 0.5H21.446C21.446 4.1 24.5181 7.22 28.9419 8.9H0V10.1H28.819C24.3952 11.78 21.3232 14.9 21.3232 18.5H22.552C22.552 13.82 28.4503 10.1 35.8232 10.1V8.9C28.5732 8.9 22.6749 5.18 22.6749 0.5Z"
                        fill="white" />
                </svg>

                <svg width="36" height="19" viewBox="0 0 36 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M22.6749 0.5H21.446C21.446 4.1 24.5181 7.22 28.9419 8.9H0V10.1H28.819C24.3952 11.78 21.3232 14.9 21.3232 18.5H22.552C22.552 13.82 28.4503 10.1 35.8232 10.1V8.9C28.5732 8.9 22.6749 5.18 22.6749 0.5Z"
                        fill="white" />
                </svg>
            </div>
        </a>

        <a href='' class="product__page__btn">
            <div class="product__page__btn__text">Wildberries</div>
            <div class="product__page__btn__icon">
                <svg width="36" height="19" viewBox="0 0 36 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M22.6749 0.5H21.446C21.446 4.1 24.5181 7.22 28.9419 8.9H0V10.1H28.819C24.3952 11.78 21.3232 14.9 21.3232 18.5H22.552C22.552 13.82 28.4503 10.1 35.8232 10.1V8.9C28.5732 8.9 22.6749 5.18 22.6749 0.5Z"
                        fill="white" />
                </svg>

                <svg width="36" height="19" viewBox="0 0 36 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M22.6749 0.5H21.446C21.446 4.1 24.5181 7.22 28.9419 8.9H0V10.1H28.819C24.3952 11.78 21.3232 14.9 21.3232 18.5H22.552C22.552 13.82 28.4503 10.1 35.8232 10.1V8.9C28.5732 8.9 22.6749 5.18 22.6749 0.5Z"
                        fill="white" />
                </svg>
            </div>
        </a>
    </div>

    <div class="product__page__info__list">
        <div class="product__page__info__row">
            <div class="product__page__info__row__title">Объём:</div>
            <div class="product__page__info__row__content">800 г</div>
        </div>

        <div class="product__page__info__row">
            <div class="product__page__info__row__title">Аромат:</div>
            <div class="product__page__info__row__content">Алоэ и гартензия</div>
        </div>

        <div class="product__page__info__row">
            <div class="product__page__info__row__title">Описание: </div>
            <div class="product__page__info__row__content">
                <p>Экологичный гипоаллергенный порошок для стирки детских вещей от Naturtek – природная забота о самых
                    маленьких. </p>

                <p>Порошок не содержит фосфатов, фосфанатов и других агрессивных химических компонентов. Разработан на
                    основе натурального мыла, соли, растительных масел и безопасных смягчителей воды.</p>

                <p>Благодаря продуманному составу, порошок Naturtek для стирки детского белья эффективно отстирывает все
                    виды загрязнений даже в холодной воде и полностью выполаскивается из белья, оставляя лишь легкий
                    аромат свежести. Сохраняет текстуру ткани и форму изделий.</p>

                <p>Формула средства также содержит безопасный натуральный смягчитель воды, предотвращающий образование
                    накипи. Благодаря этому вам больше не нужно использовать при стирке дополнительные средства для
                    защиты стиральной машины от известкового налета.</p>

                <p>Инновационный для сыпучего стирального порошка формат упаковки от Naturtek – герметичный флакон с
                    колпачком-дозатором – позволяет удобно отмерять необходимое количество средства, исключает
                    просыпание и минимизирует пыление.</p>
            </div>
        </div>
    </div>

    <div class="product__page__trigger__list">
        <div class="product__page__trigger__item">
            <div class="product__page__trigger__item__image">
                <img src="img/pages/product/trigger/1.svg?1">
            </div>
        </div>
        <div class="product__page__trigger__item">
            <div class="product__page__trigger__item__image">
                <img src="img/pages/product/trigger/2.svg?1">
            </div>
        </div>
        <div class="product__page__trigger__item">
            <div class="product__page__trigger__item__image">
                <img src="img/pages/product/trigger/3.svg?1">
            </div>
        </div>
        <div class="product__page__trigger__item">
            <div class="product__page__trigger__item__image">
                <img src="img/pages/product/trigger/4.svg?1">
            </div>
        </div>
    </div>

    <div class="product__page__tabs">
        <div class="product__page__tab js__product__page__tab">
            <div class="product__page__tab__view js_product__page__tab__btn">
                Состав
                <button class='product__page__tab__btn'></button>
            </div>
            <div class="product__page__tab__content">
                <p>Сода, кальцинированная 15-30%, соль пищевая поваренная 15-30%, перкарбонат натрия 5-15%, НПАВ 5-15%, АПАВ менее 5%, мыло натуральное (на основе пальмового и кокосового масел) менее 5%. </p>
            </div>
        </div>

        <div class="product__page__tab js__product__page__tab">
            <div class="product__page__tab__view js_product__page__tab__btn">
                Способ применения
                <button class='product__page__tab__btn'></button>
            </div>
            <div class="product__page__tab__content">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis quos voluptas animi, fugit temporibus optio quae officia expedita fuga nam modi velit laboriosam ipsam, illo rem quaerat provident? Delectus, placeat.</p>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, quo.</p>
            </div>
        </div>
    </div>
</div>
